import businessLayer.MenuItem;
import businessLayer.Restaurant;
import businessLayer.Waiter;
import dataLayer.RestaurantSerializator;
import presentationLayer.*;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        String file = args[0];
        RestaurantSerializator serializator = new RestaurantSerializator();
        Restaurant restaurant = new Restaurant();
        try {
            ArrayList<MenuItem> menu = serializator.deserialize(file);
            restaurant.setMenuItems(menu);
        }
        catch(Exception e) {
            System.out.println("Can't deserialize");
        }
        WaiterGUI waiter = new WaiterGUI(restaurant);
        AdministratorGUI administratorGUI = new AdministratorGUI(restaurant, waiter);
        ChefGUI chef = new ChefGUI();
        restaurant.attachObserver(chef);
        waiter.setWindowListener(restaurant,file);
        administratorGUI.setWindowListener(restaurant,file);
        chef.setWindowListener(restaurant,file);


    }
}
