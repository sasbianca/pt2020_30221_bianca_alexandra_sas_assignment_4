package businessLayer;

import java.util.ArrayList;
import java.util.Date;

public interface IRestaurantProcessing {
    /**
     * @pre name!= null
     * @pre price > 0
     * @post menuItems.getSize() == menuItems.getSize()@pre + 1
     * creates a baseProduct and adds it to the MenuItem list in Restaurant
     * @param name name of baseProduct
     * @param price price of baseProduct
     */
    public void createBaseProduct(String name, float price);

    /**
     * @pre menuItem != null
     * @post menuItems.getSize() == menuItems.getSize()@pre - 1
     * deletes a baseProduct from the MenuItem list in Restaurant
     * @param menuItem the object to delete
     */
    public void deleteBaseProduct(MenuItem menuItem);

    /**
     * @pre baseProduct != null
     * @pre name != 0
     * @pre price > 0
     * @post menuItems.getSize() == menuItems.getSize()@pre
     * edits a baseProduct from the MenuItem list in Restaurant
     * @param baseProduct the baseProduct to edit
     * @param name the new name
     * @param price the new price
     */
    public void editBaseProduct(BaseProduct baseProduct, String name, float price);

    /**
     * @pre name != 0
     * @pre menuItems != null
     * @pre menuItems.getSize() != 0
     * creates a compositeProduct and adds it to the MenuItem list in Restaurant
     * @param name the name
     * @param menuItems the list of MenuItems in the compositeProduct
     */
    public void createCompositeProduct(String name, ArrayList<MenuItem> menuItems);

    /**
     * @pre menuItems.getSize() != 0
     * @post restaurant.menuItems.getSize() == restaurnat.menuItems.getSize()@pre - 1
     * deletes a compositeProduct and deletes it from the MenuItem list in Restaurant
     * @param menuItem the compositeProduct to delete
     */
    public void deleteCompositeProduct(MenuItem menuItem);

    /**
     * @pre compositeProduct != null
     * @pre name != 0
     * @pre menuItems != null
     * @post restaurant.menuItems.getSize() == restaurant.getSize()@pre
     * edits a compositeProduct from the MenuItem list in Restaurnat
     * @param compositeProduct compositeProduct to edit
     * @param name new name
     * @param menuItems new menuItems list for compositeProduct
     */
    public void editCompositeProduct(CompositeProduct compositeProduct, String name, ArrayList<MenuItem> menuItems);

    /**
     * @pre order != null
     * @pre menuItem != null
     * @pre menuItem.getSize() != 0
     * @post orderArrayList.getSize() == orderArrayList.getSize()@pre + 1
     * @post orders.getSize() == orders.getSize()@pre + 1
     * create an Order and add it to the orderArrayList in Restaurant, create an entry for orders in Restaurant
     * @param order order to add
     * @param menuItem list of MenuItem to add
     */
    public void createOrder(Order order, ArrayList<MenuItem> menuItem);

    /**
     * @pre idOrder > 0
     * @post price > 0
     * computes price for an order
     * @param idOrder id of order to computer
     * @return
     */
    public float computePrice(int idOrder);

    /**
     * @pre idOrder > 0
     * generates bill for an order
     * @param idOrder id of order to generate bill
     */
    public void generateBill(int idOrder);

}
