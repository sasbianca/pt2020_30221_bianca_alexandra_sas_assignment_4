package businessLayer;

import java.util.Date;
import java.util.Objects;

public class Order {
    private int orderId;
    private Date date;
    private int table;
    static int id = 0;

    public Order(int table){
        id++;
        orderId = id;
        this.date = new Date();
        this.table = table;
    }

    public int hashCode(){
        return Objects.hash(orderId,date,table);
    }

    public int getOrderId() {
        return orderId;
    }

    public Date getDate() {
        return date;
    }

    public int getTable() {
        return table;
    }

    public int getId() {
        return id;
    }
}
