package businessLayer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
    ArrayList<MenuItem> menuItems;

    public CompositeProduct(String name, ArrayList<MenuItem> menuItems) {
        this.name = new String(name);
        this.menuItems = new ArrayList<>();
        this.menuItems.addAll(menuItems);
    }

    @Override
    public float computePrice() {
        float sum = 0;
        for(MenuItem i: menuItems)
            sum += i.computePrice();
        return sum;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setMenuItems(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public void setName(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }
}
