package businessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
    protected String name;

    public abstract float computePrice();
    public abstract String getName();


}
