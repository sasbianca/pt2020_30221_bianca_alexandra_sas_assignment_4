package businessLayer;

public class BaseProduct extends MenuItem {
    protected float price;
    public BaseProduct(String name, float price) {
        this.name = new String(name);
        this.price = price;
    }

    @Override
    public float computePrice() {
        return price;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
