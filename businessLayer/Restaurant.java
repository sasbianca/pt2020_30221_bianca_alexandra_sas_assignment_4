package businessLayer;

import dataLayer.BillGenerator;
import presentationLayer.ChefGUI;

import javax.lang.model.type.ArrayType;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * @invariant isWellFormed()
 */

public class Restaurant extends Observable implements IRestaurantProcessing {
    HashMap<Order, ArrayList<MenuItem>> orders;
    ArrayList<Order> orderArrayList;
    ArrayList<MenuItem> menuItems;
    Observer observer;

    public Restaurant(){
        super();
        orders = new HashMap<Order, ArrayList<MenuItem>>();
        menuItems = new ArrayList<MenuItem>();
        orderArrayList = new ArrayList<Order>();
    }

    protected boolean isWellFormed(){
        if(orders == null)
            return false;
        if(orderArrayList == null)
            return false;
        if(menuItems == null)
            return false;
        for(Order order: orderArrayList){
            if(orders.get(order).isEmpty())
                return false;
        }
        return true;
    }


    @Override
    public void createBaseProduct(String name, float price) {
        assert(name != null);
        assert(price >= 0);
        int size = menuItems.size();
        BaseProduct baseProduct = new BaseProduct(name, price);
        menuItems.add(baseProduct);
        assert(menuItems.size() == size+1);
        assert(isWellFormed());
    }

    @Override
    public void deleteBaseProduct(MenuItem menuItem) {
        assert(menuItem != null);
        int size = menuItems.size();
        this.menuItems.remove(menuItem);
        Iterator j = this.menuItems.iterator();
        while(j.hasNext()){
            MenuItem compositeProduct = (MenuItem) j.next();
            if(compositeProduct instanceof CompositeProduct){
                if(((CompositeProduct) compositeProduct).getMenuItems().contains(menuItem)){
                    j.remove();
                }
            }
        }
        assert(menuItems.size() == size-1);
        assert(isWellFormed());
    }

    @Override
    public void editBaseProduct(BaseProduct baseProduct, String name, float price) {
        assert(baseProduct != null);
        assert(name != null);
        assert(price > 0);
        int size = menuItems.size();
        baseProduct.name = name;
        baseProduct.price = price;
        assert(menuItems.size() == size);
        assert(isWellFormed());
    }

    @Override
    public void createCompositeProduct(String name, ArrayList<MenuItem> menuItems) {
        assert(name != null);
        assert(menuItems != null);
        int size = this.menuItems.size();
        CompositeProduct compositeProduct = new CompositeProduct(name,menuItems);
        this.menuItems.add(compositeProduct);
        assert(this.menuItems.size() == size + 1);
        assert(isWellFormed());
    }

    @Override
    public void deleteCompositeProduct(MenuItem menuItem) {
        assert(menuItem != null);
        int size = menuItems.size();
        menuItems.remove(menuItem);
        assert(menuItems.size() == size - 1);
        assert(isWellFormed());
    }

    @Override
    public void editCompositeProduct(CompositeProduct compositeProduct, String name, ArrayList<MenuItem> menuItems) {
        assert(compositeProduct != null);
        assert(name != null);
        assert(menuItems != null);
        int size = this.menuItems.size();
        compositeProduct.getMenuItems().clear();
        compositeProduct.name = name;
        compositeProduct.menuItems.addAll(menuItems);
        assert(this.menuItems.size() == size);
        assert(isWellFormed());
    }

    @Override
    public void createOrder(Order order, ArrayList<MenuItem> menuItem) {
        assert(order != null);
        assert(menuItem != null);
        assert(menuItem.size() != 0);
        int sizeOrderArrayList = orderArrayList.size();
        int sizeOrders = orders.size();
        this.orderArrayList.add(order);
        this.orders.put(order, menuItem);
        notifyObserver(this, order);
        assert(orderArrayList.size() == sizeOrderArrayList+1);
        assert(orders.size() == sizeOrders+1);
        assert(isWellFormed());

    }

    @Override
    public float computePrice(int idOrder) {
        assert(idOrder > 0);
        float sum = 0;
        for(Order order: this.orderArrayList){
            if(order.getOrderId() == idOrder){
                for(MenuItem i: orders.get(order)){
                    sum += i.computePrice();
                }
            }
        }
        assert(sum > 0);
        assert(isWellFormed());
        return sum;
    }

    @Override
    public void generateBill(int idOrder) {
        assert(idOrder > 0);
        ArrayList<MenuItem> menuItem = new ArrayList<>();
        int table = 0;
        for(Order order: this.orderArrayList){
            if(order.getOrderId() == idOrder){
                menuItem = this.orders.get(order);
                table = order.getTable();
            }
        }
        try {
            BillGenerator.bill(idOrder,table,menuItem);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert(isWellFormed());

    }

    public void notifyObserver(Observable observable, Order order){
        System.out.println("Notify chef");
        observer.update(observable, order);
    }

    public businessLayer.MenuItem findMenuItem(String name){
        for(businessLayer.MenuItem i: this.getMenuItems()){
            if(i.getName().equalsIgnoreCase(name)){
                return i;
            }
        }
        return null;
    }

    public void attachObserver(Observer observer){
        this.observer = observer;
    }

    public void removeObserver(Observer observer){
        this.observer = null;
    }

    public HashMap<Order, ArrayList<MenuItem>> getOrders() {
        return orders;
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public ArrayList<Order> getOrderArrayList() {
        return orderArrayList;
    }


}
