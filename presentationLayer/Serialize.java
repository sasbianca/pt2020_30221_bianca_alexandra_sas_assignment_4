package presentationLayer;

import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

public class Serialize implements WindowListener {
    private Restaurant restaurant;
    private String file;

    public Serialize(Restaurant restaurant, String file) {
        this.restaurant = restaurant;
        this.file = file;

    }
    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        RestaurantSerializator restaurantSerializator = new RestaurantSerializator();
        try {
            restaurantSerializator.serialize(restaurant.getMenuItems(), file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }
}
