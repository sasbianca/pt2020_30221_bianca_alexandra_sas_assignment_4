package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.Order;
import businessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateBPGUI extends JFrame {
    JLabel nameLabel = new JLabel("name");
    JLabel priceLabel = new JLabel("price");
    JTextField name = new JTextField(10);
    JTextField price = new JTextField(10);
    JButton okButton = new JButton("Create");
    JPanel content = new JPanel();

    public CreateBPGUI(Restaurant restaurant, WaiterGUI gui, AdministratorGUI admin){
        this.setSize(500,200);
        content.setLayout(new GridLayout(3,2));

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                restaurant.createBaseProduct(name.getText(),Float.parseFloat(price.getText()));
                gui.refresh(restaurant);
                admin.updateTable(restaurant);
            }
        });

        content.add(nameLabel);
        content.add(name);
        content.add(priceLabel);
        content.add(price);
        content.add(okButton);

        this.setContentPane(content);
        this.pack();
        this.setTitle("Create");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }


}
