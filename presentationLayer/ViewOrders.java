package presentationLayer;

import businessLayer.Restaurant;
import businessLayer.Waiter;

import javax.swing.*;
import java.awt.*;

public class ViewOrders extends JFrame {
    JCheckBox[] checkBoxes;
    JPanel content = new JPanel();

    public ViewOrders(Restaurant restaurant) {
        this.setSize(500, 200);
        content.setLayout(new FlowLayout());

        checkBoxes = new JCheckBox[restaurant.getOrderArrayList().size()];
        for(int i = 0; i < restaurant.getOrderArrayList().size(); i++){
            checkBoxes[i] = new JCheckBox(Integer.toString(restaurant.getOrderArrayList().get(i).getOrderId()));
        }

        for(int i = 0; i < restaurant.getOrderArrayList().size(); i++){
            content.add(checkBoxes[i]);
        }

        this.setContentPane(content);
        this.setTitle("Orders");
        this.pack();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);


    }

    public void refresh(Restaurant restaurant){
        content.remove(content);
        content = new JPanel();
        content.setLayout(new FlowLayout());
        checkBoxes = new JCheckBox[restaurant.getOrderArrayList().size()];
        for(int i = 0; i < restaurant.getOrderArrayList().size(); i++){
            checkBoxes[i] = new JCheckBox(Integer.toString(restaurant.getOrderArrayList().get(i).getOrderId()));
            content.add(checkBoxes[i]);
        }
        content.add(content);
        this.repaint();
        this.validate();
    }

}
