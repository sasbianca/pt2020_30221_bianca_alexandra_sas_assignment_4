package presentationLayer;

import businessLayer.*;
import businessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RectangularShape;
import java.util.Iterator;

public class DeleteBPGUI extends JFrame {
    JLabel searchNameLabel = new JLabel("search by name");
    JTextField name = new JTextField();
    JButton okButton = new JButton("Delete");
    JPanel content = new JPanel();

    public DeleteBPGUI(Restaurant restaurant, WaiterGUI waiter, AdministratorGUI admin) {
        this.setSize(500,200);
        content.setLayout(new GridLayout(4,2));

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MenuItem i = restaurant.findMenuItem(name.getText());
                restaurant.deleteBaseProduct(i);
                waiter.refresh(restaurant);
                admin.updateTable(restaurant);
            }
        });

        content.add(searchNameLabel);
        content.add(name);
        content.add(okButton);

        this.setContentPane(content);
        this.pack();
        this.setTitle("Delete");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);



    }

}
