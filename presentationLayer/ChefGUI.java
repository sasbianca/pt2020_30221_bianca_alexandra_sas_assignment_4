package presentationLayer;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;


import javax.swing.*;
import javax.swing.table.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI extends JFrame implements Observer {
    private JTable table;
    private String[] columns = {"id","date","table","product"};

    public ChefGUI(){
        this.setTitle("ChefGUI Orders");
        DefaultTableModel tableModel = new DefaultTableModel();
        for(int i = 0; i < columns.length; i++)
            tableModel.addColumn(columns[i]);
        table = new JTable(tableModel);
        this.add(new JScrollPane(table));

        this.setSize(500, 200);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    @Override
    public void update(Observable observable, Object o) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        String id = Integer.toString(((Order)o).getOrderId());
        String date =((Order)o).getDate().toString();
        String table = Integer.toString(((Order)o).getTable());
        ArrayList<MenuItem> menuItems = ((Restaurant) observable).getOrders().get(o);
        StringBuilder menu = new StringBuilder();
        for(MenuItem i: menuItems){
            menu.append(" ").append(i.toString());
        }

        model.addRow(new Object[]{id, date, table, menu.toString()});
    }

    public void setWindowListener(Restaurant restaurant, String file){
        this.addWindowListener(new Serialize(restaurant, file));
    }
}
