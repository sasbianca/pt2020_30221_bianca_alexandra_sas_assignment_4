package presentationLayer;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import businessLayer.Waiter;
import dataLayer.BillGenerator;
import javafx.scene.control.RadioButton;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class WaiterGUI extends JFrame {
    private JLabel orderLabel = new JLabel("Create order");
    private JButton orderButton = new JButton("create");
    private JButton priceButton = new JButton("Compute price");
    private JButton billButton = new JButton("Generate bill");
    JLabel tableLabel = new JLabel("Table nr");
    JLabel menuLabel = new JLabel("Menu");
    JTextField table = new JTextField(10);
    JTextField price = new JTextField(10);
    JCheckBox[] checkBoxes;
    private JPanel content = new JPanel();
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JPanel panel4 = new JPanel();
    private JPanel panel5 = new JPanel();
    JLabel chooseOrderLabel = new JLabel("Choose order to compute price or generate bill");
    JRadioButton[] radioButtons;
    private JButton tableButton = new JButton("View orders");
    private JFrame tableFrame = new JFrame("Orders");
    private String[] columns = {"id", "date", "table", "product"};
    private JPanel panel = new JPanel();
    private JTable tableOrders;

    public WaiterGUI(Restaurant restaurant) {
        this.setSize(500, 200);
        content.setLayout(new GridLayout(5, 1));

        checkBoxes = new JCheckBox[restaurant.getMenuItems().size()];
        for (int i = 0; i < restaurant.getMenuItems().size(); i++) {
            checkBoxes[i] = new JCheckBox(restaurant.getMenuItems().get(i).getName());
        }

        radioButtons = new JRadioButton[restaurant.getOrderArrayList().size()];
        for (int i = 0; i < restaurant.getOrderArrayList().size(); i++) {
            radioButtons[i] = new JRadioButton(Integer.toString(restaurant.getOrderArrayList().get(i).getOrderId()));
        }


        ordersView(restaurant);

        tableButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                tableFrame.setVisible(true);
            }
        });


        panel1.setLayout(new GridLayout(2, 2));
        panel1.add(priceButton);
        panel1.add(price);
        panel1.add(billButton);
        panel1.add(tableButton);


        panel2.setLayout(new FlowLayout());
        panel2.add(chooseOrderLabel);

        panel4.setLayout(new GridLayout(5, 2));
        panel4.add(orderLabel);
        panel4.add(tableLabel);
        panel4.add(table);
        panel4.add(orderLabel);
        panel4.add(orderButton);
        panel4.add(menuLabel);

        for (int i = 0; i < restaurant.getMenuItems().size(); i++) {
            panel5.add(checkBoxes[i]);
        }

        for (int i = 0; i < restaurant.getOrderArrayList().size(); i++) {
            panel3.add(radioButtons[i]);
        }

        orderGUI(restaurant, this);
        priceGUI(restaurant, this);
        billGUI(restaurant, this);

        content.add(panel1);
        content.add(panel2);
        content.add(panel3);
        content.add(panel4);
        content.add(panel5);

        this.setContentPane(content);
        this.setTitle("Waiter");
        this.pack();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    public void orderGUI(Restaurant restaurant, WaiterGUI gui) {
        orderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Order order;

                if (table.getText().equals("")) {
                    System.out.println("Can't input null table order");
                    return;
                }

                order = new Order(Integer.parseInt(table.getText()));

                ArrayList<businessLayer.MenuItem> orderItems = new ArrayList<>();
                for (int i = 0; i < restaurant.getMenuItems().size(); i++) {
                    if (checkBoxes[i].isSelected()) {
                        MenuItem item = restaurant.findMenuItem(checkBoxes[i].getText());
                        orderItems.add(item);
                    }
                }

                restaurant.createOrder(order, orderItems);
                refreshOrder(restaurant);
                updateTable(restaurant);
            }
        });
    }

    public void priceGUI(Restaurant restaurant, WaiterGUI gui) {
        priceButton.addActionListener((new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int idOrder = 0;
                for (int i = 0; i < restaurant.getOrderArrayList().size(); i++) {
                    if (radioButtons[i].isSelected()) {
                        idOrder = Integer.parseInt(radioButtons[i].getText());
                    }
                }

                price.setText(Float.toString(restaurant.computePrice(idOrder)));

            }
        }));
    }

    public void billGUI(Restaurant restaurant, WaiterGUI gui) {
        billButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int idOrder = 0;
                int table = 0;
                for (int i = 0; i < restaurant.getOrderArrayList().size(); i++) {
                    if (radioButtons[i].isSelected()) {
                        idOrder = Integer.parseInt(radioButtons[i].getText());
                    }
                }

                restaurant.generateBill(idOrder);

            }
        });
    }

    public void refresh(Restaurant restaurant) {
        content.remove(panel5);
        panel5 = new JPanel();
        panel5.setLayout(new FlowLayout());
        checkBoxes = new JCheckBox[restaurant.getMenuItems().size()];
        for (int i = 0; i < restaurant.getMenuItems().size(); i++) {
            checkBoxes[i] = new JCheckBox(restaurant.getMenuItems().get(i).getName());
            panel5.add(checkBoxes[i]);
        }
        content.add(panel5);
        this.repaint();
        this.validate();

    }

    public void refreshOrder(Restaurant restaurant) {
        content.remove(panel3);
        content.remove(panel4);
        content.remove(panel5);
        panel3 = new JPanel();
        panel4 = new JPanel();
        panel5 = new JPanel();
        checkBoxes = new JCheckBox[restaurant.getMenuItems().size()];
        for (int i = 0; i < restaurant.getMenuItems().size(); i++) {
            checkBoxes[i] = new JCheckBox(restaurant.getMenuItems().get(i).getName());
            panel5.add(checkBoxes[i]);
        }

        radioButtons = new JRadioButton[restaurant.getOrderArrayList().size()];
        for (int i = 0; i < restaurant.getOrderArrayList().size(); i++) {
            radioButtons[i] = new JRadioButton(Integer.toString(restaurant.getOrderArrayList().get(i).getOrderId()));
            panel3.add(radioButtons[i]);
        }

        panel4.setLayout(new GridLayout(5, 2));
        panel4.add(orderLabel);
        panel4.add(tableLabel);
        panel4.add(table);
        panel4.add(orderLabel);
        panel4.add(orderButton);
        panel4.add(menuLabel);

        content.add(panel3);
        content.add(panel4);
        content.add(panel5);
        this.repaint();
        this.validate();

    }

    public void ordersView(Restaurant restaurant) {

        tableFrame = new JFrame("View table");
        DefaultTableModel tableModel = new DefaultTableModel();
        for (int i = 0; i < columns.length; i++)
            tableModel.addColumn(columns[i]);
        tableOrders = new JTable(tableModel);
        panel.add(new JScrollPane(tableOrders));

        DefaultTableModel model = (DefaultTableModel) tableOrders.getModel();
        for(Order o: restaurant.getOrderArrayList()) {
            String id = Integer.toString(((Order) o).getId());
            String date = ((Order) o).getDate().toString();
            String table = Integer.toString(((Order) o).getTable());
            ArrayList<MenuItem> menuItems = restaurant.getOrders().get(o);
            StringBuilder menu = new StringBuilder();
            for (MenuItem i : menuItems) {
                menu.append(" ").append(i.toString());
            }
            model.addRow(new Object[]{id, date, table, menu.toString()});
        }

        tableFrame.setContentPane(panel);
        tableFrame.setSize(500,500);
        tableFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        tableFrame.setVisible(false);

    }

    public void updateTable(Restaurant restaurant) {

        tableFrame.remove(panel);
        panel = new JPanel();
        DefaultTableModel tableModel = new DefaultTableModel();
        for (int i = 0; i < columns.length; i++)
            tableModel.addColumn(columns[i]);
        tableOrders = new JTable(tableModel);
        panel.add(new JScrollPane(tableOrders));

        DefaultTableModel model = (DefaultTableModel) tableOrders.getModel();
        for(Order o: restaurant.getOrderArrayList()) {
            String id = Integer.toString(((Order) o).getOrderId());
            String date = ((Order) o).getDate().toString();
            String table = Integer.toString(((Order) o).getTable());
            ArrayList<MenuItem> menuItems = restaurant.getOrders().get(o);
            StringBuilder menu = new StringBuilder();
            for (MenuItem i : menuItems) {
                menu.append(" ").append(i.toString());
            }
            model.addRow(new Object[]{id, date, table, menu.toString()});
        }

        tableFrame.setContentPane(panel);
        tableFrame.repaint();
        tableFrame.validate();

    }

    public void setWindowListener(Restaurant restaurant, String file){
        this.addWindowListener(new Serialize(restaurant, file));
    }


}

