package presentationLayer;

import businessLayer.Administrator;
import businessLayer.BaseProduct;
import businessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditBPGUI extends JFrame {
    JLabel newNameLabel = new JLabel("name");
    JLabel newPriceLabel = new JLabel("price");
    JLabel searchNameLabel = new JLabel("search by name");
    JTextField oldName = new JTextField(10);
    JTextField newName = new JTextField(10);
    JTextField newPrice = new JTextField(10);
    JButton okButton = new JButton("Edit");
    JPanel content = new JPanel();

    public EditBPGUI(Restaurant restaurant, WaiterGUI waiter, AdministratorGUI admin) {
        this.setSize(500,200);
        content.setLayout(new GridLayout(4,2));

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BaseProduct baseProduct = (BaseProduct) restaurant.findMenuItem(oldName.getText());
                restaurant.editBaseProduct(baseProduct,newName.getText(),Float.parseFloat(newPrice.getText()));
                waiter.refresh(restaurant);
                admin.updateTable(restaurant);
            }
        });

        content.add(searchNameLabel);
        content.add(oldName);
        content.add(newNameLabel);
        content.add(newName);
        content.add(newPriceLabel);
        content.add(newPrice);
        content.add(okButton);

        this.setContentPane(content);
        this.pack();
        this.setTitle("Edit");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);

    }
}

