package presentationLayer;

import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import businessLayer.Waiter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class EditCPGUI extends JFrame {
    JLabel newNameLabel = new JLabel("name");
    JLabel searchNameLabel = new JLabel("search by name");
    JTextField oldName = new JTextField(10);
    JTextField newName = new JTextField(10);
    JButton okButton = new JButton("Edit");
    JPanel content = new JPanel();
    JLabel newComponentsLabel = new JLabel("components");
    JCheckBox[] checkBoxes;
    JPanel panel1 = new JPanel();
    JPanel panel2 = new JPanel();
    JPanel panel3 = new JPanel();

    public EditCPGUI(Restaurant restaurant, WaiterGUI waiter, AdministratorGUI admin) {
        this.setSize(500,500);
        content.setLayout(new GridLayout(4,2));

        checkBoxes = new JCheckBox[restaurant.getMenuItems().size()];
        for(int i = 0; i < restaurant.getMenuItems().size(); i++){
            checkBoxes[i] = new JCheckBox(restaurant.getMenuItems().get(i).getName());
        }

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CompositeProduct compositeProduct = (CompositeProduct) restaurant.findMenuItem(oldName.getText());
                ArrayList<MenuItem> menuItems = new ArrayList<>();
                for(int i = 0; i < restaurant.getMenuItems().size(); i++){
                    if(checkBoxes[i].isSelected()){
                        businessLayer.MenuItem menuItem = restaurant.findMenuItem(checkBoxes[i].getText());
                        menuItems.add(menuItem);
                    }
                }

                restaurant.editCompositeProduct(compositeProduct,newName.getText(),menuItems);
                waiter.refresh(restaurant);
                refresh(restaurant);
                admin.updateTable(restaurant);
            }
        });

        panel1.setLayout(new GridLayout(3,2));
        panel1.add(searchNameLabel);
        panel1.add(oldName);
        panel1.add(newNameLabel);
        panel1.add(newName);
        panel1.add(newComponentsLabel);

        panel3.setLayout(new FlowLayout());
        for(int i = 0; i < restaurant.getMenuItems().size(); i++){
            panel3.add(checkBoxes[i]);
        }

        panel2.setLayout(new FlowLayout());
        panel2.add(okButton);

        content.add(panel1);
        content.add(panel2);
        content.add(panel3);

        this.setContentPane(content);
        this.pack();
        this.setTitle("Edit");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);

    }

    public void refresh(Restaurant restaurant) {
        content.remove(panel3);
        panel3 = new JPanel();
        panel3.setLayout(new FlowLayout());
        checkBoxes = new JCheckBox[restaurant.getMenuItems().size()];
        for (int i = 0; i < restaurant.getMenuItems().size(); i++) {
            checkBoxes[i] = new JCheckBox(restaurant.getMenuItems().get(i).getName());
            panel3.add(checkBoxes[i]);
        }
        content.add(panel3);
        this.repaint();
        this.validate();
    }
}
