package presentationLayer;

import businessLayer.*;
import businessLayer.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdministratorGUI extends JFrame {
    private JButton addBaseProduct = new JButton("Add base product");
    private JButton addCompositeProduct = new JButton("Add a composite product");
    private JButton editBaseProduct = new JButton("Edit base product");
    private JButton editCompositeProduct = new JButton("Edit composite product");
    private JButton deleteBaseProduct = new JButton("Delete base product");
    private JButton deleteCompositeProduct = new JButton("Delete composite product");
    private JPanel content = new JPanel();
    private JTable table;
    private String[] columns = {"name", "price"};
    private JPanel panel = new JPanel();
    private JFrame tableFrame;
    private JButton tableButton = new JButton("View menu item table");

    public AdministratorGUI(Restaurant restaurant, WaiterGUI gui) {
        this.setSize(500, 500);
        content.setLayout(new GridLayout(7,1));

        createBPGUI(restaurant, gui, this);
        createCPGUI(restaurant, gui, this);
        editBPGUI(restaurant, gui, this);
        editCPGUI(restaurant, gui, this);
        deleteBPGUI(restaurant, gui, this);
        deleteCPGUI(restaurant, gui, this);
        menuItemView(restaurant);
        tableButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                tableFrame.setVisible(true);
            }
        });

        content.add(addBaseProduct);
        content.add(editBaseProduct);
        content.add(deleteBaseProduct);
        content.add(addCompositeProduct);
        content.add(editCompositeProduct);
        content.add(deleteCompositeProduct);
        content.add(tableButton);

        this.setContentPane(content);
        this.setTitle("Administrator");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void createBPGUI(Restaurant restaurant, WaiterGUI gui, AdministratorGUI admin) {
        addBaseProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreateBPGUI createBPGUI = new CreateBPGUI(restaurant, gui, admin);
            }
        });
    }

    public void editBPGUI(Restaurant restaurant, WaiterGUI gui, AdministratorGUI admin) {
        editBaseProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EditBPGUI editBPGUI = new EditBPGUI(restaurant, gui, admin);

            }
        });
    }

    public void deleteBPGUI(Restaurant restaurant, WaiterGUI gui, AdministratorGUI admin) {
        deleteBaseProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DeleteBPGUI deleteBPGUI = new DeleteBPGUI(restaurant, gui, admin);

            }
        });
    }

    public void createCPGUI(Restaurant restaurant, WaiterGUI gui, AdministratorGUI admin) {
        addCompositeProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreateCPGUI createCPGUI = new CreateCPGUI(restaurant, gui, admin);

            }
        });
    }

    public void editCPGUI(Restaurant restaurant, WaiterGUI gui, AdministratorGUI admin) {
        editCompositeProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EditCPGUI editCPGUI = new EditCPGUI(restaurant, gui, admin);

            }
        });
    }

    public void deleteCPGUI(Restaurant restaurant, WaiterGUI gui, AdministratorGUI admin) {
        deleteCompositeProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DeleteCPGUI deleteCPGUI = new DeleteCPGUI(restaurant, gui, admin);

            }
        });
    }

    public void menuItemView(Restaurant restaurant) {

        tableFrame = new JFrame("View table");
        DefaultTableModel tableModel = new DefaultTableModel();
        for (int i = 0; i < columns.length; i++)
            tableModel.addColumn(columns[i]);
        table = new JTable(tableModel);
        panel.add(new JScrollPane(table));

        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for(MenuItem i: restaurant.getMenuItems()){
            model.addRow(new Object[]{i.getName(), Float.toString(i.computePrice())});
        }

        tableFrame.setContentPane(panel);
        tableFrame.setSize(500,500);
        tableFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        tableFrame.setVisible(false);

    }

    public void updateTable(Restaurant restaurant) {

        tableFrame.remove(panel);
        panel = new JPanel();
        DefaultTableModel tableModel = new DefaultTableModel();
        for (int i = 0; i < columns.length; i++)
            tableModel.addColumn(columns[i]);
        table = new JTable(tableModel);
        panel.add(new JScrollPane(table));

        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for(MenuItem i: restaurant.getMenuItems()){
            model.addRow(new Object[]{i.getName(), Float.toString(i.computePrice())});
        }

        tableFrame.setContentPane(panel);
        tableFrame.repaint();
        tableFrame.validate();

    }


    public void setWindowListener(Restaurant restaurant, String file){
        this.addWindowListener(new Serialize(restaurant, file));
    }




}




