package presentationLayer;

import businessLayer.Administrator;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeleteCPGUI extends JFrame {
    JLabel searchNameLabel = new JLabel("search by name");
    JTextField searchName = new JTextField(10);
    JButton okButton = new JButton("Delete");
    JPanel content = new JPanel();

    public DeleteCPGUI(Restaurant restaurant, WaiterGUI waiter, AdministratorGUI admin) {
        this.setSize(500,500);
        content.setLayout(new GridLayout(2,2));

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MenuItem i = restaurant.findMenuItem(searchName.getText());
                restaurant.deleteCompositeProduct(i);
                waiter.refresh(restaurant);
                admin.updateTable(restaurant);
            }
        });

        content.add(searchNameLabel);
        content.add(searchName);
        content.add(okButton);

        this.setContentPane(content);
        this.pack();
        this.setTitle("Delete");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }
}
