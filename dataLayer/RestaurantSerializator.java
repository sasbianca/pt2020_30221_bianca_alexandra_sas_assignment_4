package dataLayer;

import businessLayer.MenuItem;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

public class RestaurantSerializator {


    public ArrayList<MenuItem> deserialize(String file) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(file);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        ArrayList<MenuItem> menuItems = (ArrayList<MenuItem>)in.readObject();
        in.close();
        fileIn.close();
        return menuItems;
    }

    public void serialize(ArrayList<MenuItem> menuItems, String file) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(file);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(menuItems);
        out.close();
        fileOut.close();

    }



}
