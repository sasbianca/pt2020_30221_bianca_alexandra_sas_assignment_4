package dataLayer;

import businessLayer.MenuItem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class BillGenerator {

    public static void bill(int id, int table, ArrayList<MenuItem> menuItems) throws IOException {
        StringBuilder s = new StringBuilder();
        s.append("Order").append(Integer.toString(id)).append(".txt");
        File file = new File(s.toString());
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("Id: "+id+"\n");
        fileWriter.write("Table: "+table+'\n');
        float sum = 0;
        for(MenuItem i: menuItems){
            fileWriter.write(i.getName()+" "+i.computePrice()+"\n");
            sum += i.computePrice();
        }
        fileWriter.write("Total "+sum);
        fileWriter.close();




    }
}
